package brokercomms

import (
	"bytes"
	"errors"
	"net/http"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_brokercomms"
	jacamartst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

type brokerTests struct {
	username string
	cfg      configure.Broker
	req      envparser.RequiredEnv
	reg      Registerer

	assertError      func(*testing.T, error)
	assertRegisterer func(*testing.T, Registerer)
}

func TestNewJob(t *testing.T) {
	tests := map[string]brokerTests{
		"JWT hash (sha256) created": {
			username: "user",
			cfg: configure.Broker{
				URL: "https://gitlab.example.com",
			},
			req: envparser.RequiredEnv{
				JobID:    "123",
				JobToken: "T0k3n",
				CIJobJWT: "place.holder.jwt",
			},
			assertRegisterer: func(t *testing.T, r Registerer) {
				assert.IsType(t, &ciJobJWT{}, r)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := NewJob(tt.cfg, tt.req, tt.username)

			if tt.assertRegisterer != nil {
				tt.assertRegisterer(t, got)
			}
		})
	}
}

func mockJWT(url string) Registerer {
	return NewJob(
		configure.Broker{
			URL: url,
		},
		envparser.RequiredEnv{
			JobToken: "T0k3n",
			CIJobJWT: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWxsbyI6IndvbHJkIn0.sm0r7FOR-HZe3FhWcTnYq-ZiMMNRXY03fKH8w298WeE",
		},
		"test",
	)
}

func Test_ciJobJWT_postJob(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_brokercomms.NewMockHTTPClient(ctrl)
	m.EXPECT().Do(jacamartst.NewURL("https://bad-request.localhost/-/broker/job")).
		Return(&http.Response{
			Status:     "400 Bad Request",
			StatusCode: http.StatusBadRequest,
			Body:       &jacamartst.ClosingBuffer{},
		}, nil).Times(1)
	m.EXPECT().Do(jacamartst.NewURL("https://error.localhost/-/broker/job")).
		Return(nil, errors.New("error message")).Times(1)
	m.EXPECT().Do(jacamartst.NewURL("https://json-error.localhost/-/broker/job")).Return(&http.Response{
		Status:     "201 Created",
		StatusCode: http.StatusCreated,
		Body:       &jacamartst.ClosingBuffer{bytes.NewBufferString(`{"broker":`)},
	}, nil).Times(1)
	m.EXPECT().Do(jacamartst.NewURL("https://created.localhost/-/broker/job")).
		Return(&http.Response{
			Status:     "201 Created",
			StatusCode: http.StatusCreated,
			Body:       &jacamartst.ClosingBuffer{bytes.NewBufferString(`{"broker_token":"test.broker.token"}`)},
		}, nil).Times(1)
	client = m

	tests := map[string]brokerTests{
		"bad http request (400) encountered": {
			reg: mockJWT("https://bad-request.localhost/"),
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"failed to post job data to broker, 400 Bad Request",
				)
			},
		},
		"go error encountered during http request": {
			reg: mockJWT("https://error.localhost/"),
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "error message")
			},
		},
		"job successfully registered with broker service": {
			reg: mockJWT("https://created.localhost/"),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRegisterer: func(t *testing.T, reg Registerer) {
				assert.Equal(t, "test.broker.token", reg.JobBrokerToken())
			},
		},
		"invalid URL configuration detected": {
			reg: mockJWT("something/url"),
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "failed to parse registration URL: invalid Broker URL configured")
			},
		},
		"malformed json returned": {
			reg: mockJWT("https://json-error.localhost"),
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "failed to decode response: unexpected EOF")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.reg.PostJob()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertRegisterer != nil {
				tt.assertRegisterer(t, tt.reg)
			}
		})
	}
}
