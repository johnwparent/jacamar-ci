package preparations

import (
	"io/ioutil"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

func TestConfigExec(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	runner := mock_runmechanisms.NewMockRunner(ctrl)
	runner.EXPECT().BuildsDir().Return("/builds").Times(1)
	runner.EXPECT().CacheDir().Return("/cache").Times(1)

	msg := mock_logging.NewMockMessenger(ctrl)
	// We have other more comprehensive tests that verify stateful
	// variables are properly formatted in the workflow. In this case
	// we need to just ensure that stdout is generated upon success.
	msg.EXPECT().Stdout(gomock.Any(), gomock.Any()).Times(2)
	msg.EXPECT().Stderr(gomock.Any(), gomock.Any()).Times(1)

	ll := logrus.New()
	ll.Out = ioutil.Discard
	sysLog := logrus.NewEntry(ll)

	auth := mock_authuser.NewMockAuthorized(ctrl)
	auth.EXPECT().BuildState().Return(envparser.StatefulEnv{
		BaseDir:   "/base",
		BuildsDir: "/builds",
		CacheDir:  "/cache",
		ScriptDir: "/script",
		Username:  "user",
	}).Times(1)
	auth.EXPECT().SharedBuildsDir().Return(false).Times(1)

	failAuth := mock_authuser.NewMockAuthorized(ctrl)
	failAuth.EXPECT().BuildState().Return(envparser.StatefulEnv{}).Times(1)

	tests := map[string]prepTests{
		"configuration prep success": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			ae: &executors.AbstractExecutor{
				Auth:   auth,
				Msg:    msg,
				Runner: runner,
				SysLog: sysLog,
			},
		},
		"missing stateful variables during build": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			ae: &executors.AbstractExecutor{
				Auth:   failAuth,
				Msg:    msg,
				SysLog: sysLog,
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			ConfigExec(tt.ae, tt.c, func() {})
		})
	}
}

func Test_buildState(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	passAuth := mock_authuser.NewMockAuthorized(ctrl)
	passAuth.EXPECT().BuildState().Return(envparser.StatefulEnv{
		BaseDir:   "/base",
		BuildsDir: "/builds",
		CacheDir:  "/cache",
		ScriptDir: "/script",
		Username:  "user",
	}).Times(1)

	failAuth := mock_authuser.NewMockAuthorized(ctrl)
	failAuth.EXPECT().BuildState().Return(envparser.StatefulEnv{})

	tests := map[string]prepTests{
		"missing stateful variables during build": {
			ae: &executors.AbstractExecutor{
				Auth: failAuth,
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"completed stateful variables identified": {
			ae: &executors.AbstractExecutor{
				Auth: passAuth,
			},
			assertMap: func(t *testing.T, m map[string]string) {
				req := map[string]string{
					"JACAMAR_CI_AUTH_USERNAME": "user",
					"JACAMAR_CI_BASE_DIR":      "/base",
					"JACAMAR_CI_BUILDS_DIR":    "/builds",
					"JACAMAR_CI_CACHE_DIR":     "/cache",
					"JACAMAR_CI_SCRIPT_DIR":    "/script",
				}
				assert.Equal(t, req, m)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := buildState(tt.ae)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertMap != nil {
				tt.assertMap(t, got)
			}
		})
	}
}
