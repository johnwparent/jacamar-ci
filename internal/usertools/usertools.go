// Package usertools is a generic catch-all package for Jacamar CI that is meant to support
// any range of functionality requires by CI user level job interaction, across multiple other
// internal run mechanisms and executors.
package usertools

import (
	"os"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
)

const (
	// FilePermissions for any file/directory created.
	FilePermissions = 0700
	// FileUmask is the require Umask that must be used to all directory creation.
	FileUmask = 077
)

// PreCleanupVerification is targeted to support ensuring the minimum possible user
// directories and CI environment is available before any executor/configuration
// specific cleanup actions can be taken.
func PreCleanupVerification(a authuser.Authorized) error {
	dirs := []string{a.BaseDir(), a.ScriptDir(), a.BuildsDir(), a.CacheDir()}
	for _, d := range dirs {
		if err := verifyBasePermissions(d); err != nil {
			return err
		}

		// The verifyBasePermissions() will return a nil if a directory doesn't
		// exist. In our case we will want to generate an error.
		_, err := os.Stat(d)
		if err != nil && os.IsNotExist(err) {
			return err
		}
	}
	return nil
}
