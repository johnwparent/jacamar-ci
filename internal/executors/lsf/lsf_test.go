package lsf

import (
	"errors"
	"os"
	"path/filepath"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/batch"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_batch"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

func Test_executor_Prepare1(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	miss := mock_configure.NewMockConfigurer(ctrl)
	miss.EXPECT().Batch().Return(configure.Batch{})

	path, _ := filepath.Abs("../../../test/scripts/schedulers")
	found := mock_configure.NewMockConfigurer(ctrl)
	found.EXPECT().Batch().Return(configure.Batch{
		SchedulerBin: path,
	})

	tests := map[string]struct {
		cfg         configure.Configurer
		assertError func(*testing.T, error)
	}{
		"bsub identified": {
			cfg: found,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"bsub not found": {
			cfg: miss,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to locate LSF (bsub) in the CI environment")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec: &executors.AbstractExecutor{
					Cfg:   tt.cfg,
					Stage: "prepare_exec",
				},
			}
			err := e.Prepare()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_executor_Cleanup(t *testing.T) {
	// If a cleanup phase is added this will needed updated.
	t.Run("verify default cleanup skipped", func(t *testing.T) {
		e := NewExecutor(&executors.AbstractExecutor{})
		err := e.Cleanup()
		assert.Nil(t, err, "cleanup successfully skipped")
	})
}

func Test_executor_Run(t *testing.T) {
	script := "/some/job/script.bash"
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	run := mock_runmechanisms.NewMockRunner(ctrl)
	run.EXPECT().JobScriptOutput(script).Return(nil)
	run.EXPECT().JobScriptOutput(script + ".error").Return(errors.New("error message"))
	run.EXPECT().JobScriptOutput(script, "bsub -I -N1")
	run.EXPECT().JobScriptOutput(script+".error", "bsub -I -N1").Return(errors.New("error message"))

	mng := mock_batch.NewMockManager(ctrl)
	mng.EXPECT().NFSTimeout(gomock.Eq("30s"), gomock.Any()).Times(2)
	mng.EXPECT().BatchCmd().Return("bsub").Times(2)
	mng.EXPECT().UserArgs().Return("-N1").Times(2)

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().Batch().Return(configure.Batch{
		NFSTimeout: "30s",
	}).AnyTimes()

	type fields struct {
		absExec *executors.AbstractExecutor
		mng     batch.Manager
	}
	tests := map[string]struct {
		fields      fields
		assertError func(*testing.T, error)
	}{
		"non-build job script execute successfully": {
			fields: fields{
				absExec: &executors.AbstractExecutor{
					Runner:     run,
					ScriptPath: script,
					Stage:      "prepare_script",
				},
				mng: mng,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"non-build job script execute with error": {
			fields: fields{
				absExec: &executors.AbstractExecutor{
					Runner:     run,
					ScriptPath: script + ".error",
					Stage:      "after_script",
				},
				mng: mng,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "error message")
			},
		},
		"step_script executed successfully": {
			fields: fields{
				absExec: &executors.AbstractExecutor{
					Cfg:        cfg,
					Runner:     run,
					ScriptPath: script,
					Stage:      "step_script",
				},
				mng: mng,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"build_script executed with error": {
			fields: fields{
				absExec: &executors.AbstractExecutor{
					Cfg:        cfg,
					Runner:     run,
					ScriptPath: script + ".error",
					Stage:      "build_script",
				},
				mng: mng,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "error message")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec: tt.fields.absExec,
				mng:     tt.fields.mng,
			}
			err := e.Run()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestNewExecutor(t *testing.T) {
	os.Setenv(envparser.UserEnvPrefix+"TEST_BSUB_PARAMETERS", "-e test.txt")
	defer os.Unsetenv(envparser.UserEnvPrefix + "TEST_BSUB_PARAMETERS")

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Warn(
		gomock.Eq("Illegal argument detected. Please remove: -e"),
	).Times(1)

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().Batch().Return(configure.Batch{
		NFSTimeout:        "30s",
		ArgumentsVariable: []string{"TEST_BSUB_PARAMETERS"},
	}).AnyTimes()

	type args struct {
		ae *executors.AbstractExecutor
	}
	tests := map[string]struct {
		args           args
		assertExecutor func(*testing.T, *executor)
	}{
		"new executor for step_script": {
			args: args{
				ae: &executors.AbstractExecutor{
					Cfg:   cfg,
					Msg:   msg,
					Stage: "step_script",
				},
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
			},
		},
		"new executor for after_script": {
			args: args{
				ae: &executors.AbstractExecutor{
					Stage: "after_script",
				},
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := NewExecutor(tt.args.ae)

			if tt.assertExecutor != nil {
				tt.assertExecutor(t, got)
			}
		})
	}
}
