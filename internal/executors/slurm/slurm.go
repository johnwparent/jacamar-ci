package slurm

import (
	"errors"
	"fmt"
	"os/exec"
	"sync"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/batch"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
)

type executor struct {
	absExec   *executors.AbstractExecutor
	mng       batch.Manager
	id        string
	outFile   string
	sleepTime time.Duration // Sleep between scheduler interactions.
}

const (
	// completingTimeout is the timeout enforced for jobs stuck in a COMPLETING state.
	completingTimeout = 5 * time.Minute
)

func (e *executor) Prepare() error {
	_, err := exec.LookPath(
		batch.PrefixSchedulerPath("sbatch", e.absExec.Cfg.Batch().SchedulerBin),
	)
	if err != nil {
		return errors.New("unable to locate Slurm (sbatch) in the CI environment")
	}

	return nil
}

func (e *executor) Run() error {
	if !(e.absExec.Stage == "step_script" || e.absExec.Stage == "build_script") {
		return e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath)
	}
	return e.runSlurm()
}

func (e *executor) Cleanup() error {
	// No slurm specific cleanup required.
	return nil
}

func cancelJob(cmd, jobID string, run runmechanisms.Runner) error {
	out, err := run.ReturnOutput(cmd + " " + jobID)
	if err != nil {
		return errors.New(out)
	}
	return nil
}

func (e *executor) runSlurm() error {
	if err := e.submitJob(); err != nil {
		return fmt.Errorf("error while attempting to submit job to Slurm: %w", err)
	}

	// Wait for configured NFS timeout window.
	defer e.mng.NFSTimeout((e.absExec.Cfg.Batch()).NFSTimeout, e.absExec.Msg)

	if err := e.monitorJob(); err != nil {
		return fmt.Errorf("error while attempting to monitor job (%s): %w", e.id, err)
	}
	return nil
}

// submitJob schedules the job with the underlying Slurm application, capturing the JobID.
func (e *executor) submitJob() error {
	sbatchStdin := fmt.Sprintf(
		"%s --output=%s %s",
		e.mng.BatchCmd(),
		e.outFile,
		e.mng.UserArgs(),
	)

	out, err := e.absExec.Runner.JobScriptReturn(e.absExec.ScriptPath, sbatchStdin)
	if err != nil {
		return fmt.Errorf("failed to request Slurm allocation: %s", out)
	}

	e.id, err = sbatchJobID(out)
	if err != nil {
		return fmt.Errorf("unable to identify job id")
	}
	fmt.Println(out) // Output results from sbatch for CI user.
	e.outFile = outputFile(e.id, e.outFile)

	return nil
}

// monitorJob examines the job's output logs and status throughout duration.
func (e *executor) monitorJob() error {
	var wg sync.WaitGroup
	wg.Add(3) // outfile + sacct

	jobDone := make(chan struct{})
	go func() {
		defer wg.Done()
		err := e.mng.TailFiles([]string{e.outFile}, jobDone, 45*time.Second, e.absExec.Msg)
		if err != nil {
			e.absExec.Msg.Warn("Unable to monitor files, job output distrusted: %s", err.Error())
		}
	}()

	go func() {
		defer wg.Done()
		e.monitorTermination(jobDone)
	}()

	jobErr := make(chan error, 1)
	go func() {
		defer wg.Done()
		timeOut := completingTimeout
		stdin := sacctStdin(e.mng.StateCmd(), e.id)
		err := obtainSacct(stdin, timeOut, e.sleepTime, e.absExec.Runner)
		jobErr <- err
	}()

	err := <-jobErr

	close(jobDone)
	wg.Wait()

	return err
}

// NewExecutor generates a valid Slurm executor that fulfills the executors.Executor interface.
func NewExecutor(ae *executors.AbstractExecutor) (e *executor) {
	e = &executor{
		absExec: ae,
	}

	if e.absExec.Stage == "step_script" || ae.Stage == "build_script" {
		set := batch.Settings{
			BatchCmd:    "sbatch",
			StateCmd:    "sacct",
			StopCmd:     "scancel",
			IllegalArgs: []string{"-o", "--output", "-e", "--error"},
		}
		e.mng = batch.NewBatchJob(set, ae.Cfg.Batch(), ae.Msg)

		e.sleepTime = batch.DefaultSleep
		e.outFile = fmt.Sprintf("%s/slurm-out-%%j.out", ae.Auth.ScriptDir())
	}

	return
}
