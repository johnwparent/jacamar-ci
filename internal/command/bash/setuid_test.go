package bash

import (
	"os/exec"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	jacamartst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

func TestMechanism_targetValidUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	workingShell := &shell{}
	workingShell.build("bash")

	tests := map[string]bashTests{
		"valid command generated": {
			auth: mockAuthWorkingID(ctrl),
			s:    workingShell,
			assertCommand: func(t *testing.T, cmd *exec.Cmd) {
				assert.NotNil(t, cmd)
				assert.True(t, strings.HasSuffix(cmd.Path, "bash"))
				assert.Equal(t, uint32(1001), cmd.SysProcAttr.Credential.Uid, "target UID")
				assert.Equal(t, uint32(2002), cmd.SysProcAttr.Credential.Gid, "target GID")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"error encountered by IntegerID": {
			auth: mockAuthErrorID(ctrl),
			s:    workingShell,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "error message")
			},
		},
		"improperly generate cmd, nil SysProcAttr caught": {
			auth: mockAuthWorkingID(ctrl),
			s:    &shell{cmd: nil},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"required cmd structure is nil, ensure proper generation in build()",
				)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.s.targetValidUser(tt.auth, tt.broker, tt.state)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}

			if tt.assertCommand != nil {
				tt.assertCommand(t, tt.s.cmd)
			}
		})
	}
}

func Test_safeEnv(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_authuser.NewMockAuthorized(ctrl)
	m.EXPECT().Username().Return("user").AnyTimes()

	tests := map[string]bashTests{
		"standard \"safe\" environment": {
			targetEnv: map[string]string{
				envparser.UserEnvPrefix + "A":          "foo",
				envparser.StatefulEnvPrefix + "B":      "bar",
				envparser.UserEnvPrefix + "CI_JOB_JWT": "T0k3n",
			},
			auth: m,
			assertEnv: func(t *testing.T, e []string) {
				assert.Contains(t, e, "HOME=user")
				assert.Contains(t, e, envparser.UserEnvPrefix+"A"+"=foo")
				assert.Contains(t, e, envparser.StatefulEnvPrefix+"B"+"=bar")
				assert.Contains(t, e, "PATH=/usr/bin:/bin:/usr/local/bin:/usr/local/sbin:/usr/sbin")
				assert.NotContains(t, e, "Tok3n")
			},
		},
		"broker enabled, required safe environment removes tokens": {
			targetEnv: map[string]string{
				envparser.UserEnvPrefix + "A":          "T0k3n",
				envparser.UserEnvPrefix + "CI_JOB_JWT": "T0k3n",
				"TRUSTED_CI_JOB_TOKEN":                 "T0k3n",
			},
			encoded: "e258d248fda94c63753607f7c4494ee0fcbe92f1a76bfdac795c9d84101eb317",
			broker:  true,
			auth:    m,
			assertEnv: func(t *testing.T, e []string) {
				assert.NotContains(t, e, "Tok3n")
				assert.Contains(t, e, envparser.UserEnvPrefix+"A"+"=e258d248fda94c63753607f7c4494ee0fcbe92f1a76bfdac795c9d84101eb317")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			jacamartst.SetEnv(tt.targetEnv)
			defer jacamartst.UnsetEnv(tt.targetEnv)

			got := safeEnv(tt.auth, tt.broker, tt.encoded)

			if tt.assertEnv != nil {
				tt.assertEnv(t, got)
			}
		})
	}
}
