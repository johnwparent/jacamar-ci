# Jacamar CI

[![pipeline status](https://gitlab.com/ecp-ci/jacamar-ci/badges/develop/pipeline.svg)](https://gitlab.com/ecp-ci/jacamar-ci/-/commits/develop)
[![coverage report](https://gitlab.com/ecp-ci/jacamar-ci/badges/develop/coverage.svg)](https://gitlab.com/ecp-ci/jacamar-ci/-/commits/develop)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/ecp-ci/jacamar-ci)](https://goreportcard.com/report/gitlab.com/ecp-ci/jacamar-ci)

Jacamar is the HPC focused CI/CD driver for GitLab’s
[custom executor](https://docs.gitlab.com/runner/executors/custom.html).
The core goal of this project is to establish a maintainable, yet extensively configurable
tool that will bridge the gap between GitLab’s testing model and the requirements of ECP CI.

## Documentation

For complete documentation regarding all aspects of deployment and administration of
Jacamar CI please see [ecp-ci.gitlab.io](https://ecp-ci.gitlab.io/docs/admin.html#jacamar-ci).

## Quick Start

If new to Jacamar CI we recommend viewing the
[administrator tutorial](https://ecp-ci.gitlab.io/docs/admin/jacamar/tutorial.html).

### Installation

For complete documentation on deployment as well as installation of a
supported release package please see Jacamar's
[official documentation](https://ecp-ci.gitlab.io/docs/admin/jacamar/deployment.html).

Installation from source supported via `make` and requires:

* Bash
* Git
* Go versions 1.15+ (see [official instructions](https://golang.org/doc/install))
* Make
* libc

```Console
git clone https://gitlab.com/ecp-ci/jacamar-ci.git
cd jacamar-ci
make build
make install PREFIX=/usr/local
```

Though the official GitLab Runner supports the custom executor there are several
enhancements we've made that are not available in the upstream version.
As such you will also need to build and install a patched version of the runner.
To best support this process we've created a local
[Spack](https://spack.io/) repository.

```Console
spack repo add $(pwd)/build/package/Spack/jacamar-repo
spack install gitlab-runner+jacamar+federation
```

Following the above commands will create a `gitlab-runner` package you can use
for testing. Remember in our case the version of the runner only matters
with regard to the `gitlab-runner run` command (though there will need to
be a runner available on the user's `PATH`).

Please note that once Jacamar is ready for production we aim to further limit
the modifications to the runner.

### Configuration

If you've administered a GitLab Runner before, the
[configuration](https://docs.gitlab.com/runner/configuration/advanced-configuration.html)
will be familiar. With Jacamar, it is no different as it not only relies upon
specific management of the upstream runner but also its own configuration. Both
configurations leverage [TOML](https://github.com/toml-lang/toml).

#### GitLab Runner

This example target the `jacamar-auth` application as we wish to
leverage the supported
[authorization and downscoping mechanisms](https://ecp-ci.gitlab.io/docs/admin/jacamar/auth.html).

```TOML
[[runners]]
  name = "Custom Executor Example"
  url = "https://gitlab.example.com/"
  token = "T0k3n"
  executor = "custom"
  [runners.custom]
    config_exec = "jacamar-auth"
    config_args = ["config", "--configuration", "/etc/gitlab-runner/custom-config.toml"]

    prepare_exec = "jacamar-auth"
    prepare_args = ["prepare"]

    run_exec = "jacamar-auth"
    run_args = ["run"]

    cleanup_exec = "jacamar-auth"
    cleanup_args = ["cleanup", "--configuration", "/etc/gitlab-runner/custom-config.toml"]
```

#### Jacamar

You will notice a Jacamar specific configuration is required
(`--configuration /etc/gitlab-runner/custom-config.toml`)
to account for a plethora of configuration options:

```TOML
[general]
executor = "slurm"
data_dir = "/ecp"

[auth]
downscope = "setuid"
```

Please see the [configuration documentation](https://ecp-ci.gitlab.io/docs/admin/jacamar/configuration.html)
for a complete list of advanced configuration options at your disposal.
