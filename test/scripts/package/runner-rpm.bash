#!/usr/bin/env bash

# Test and build RPM file from CI jobs.

set -eo pipefail
set -o xtrace

[ -z "$CI_PROJECT_DIR" ] && export CI_PROJECT_DIR=$(pwd)

RUNNER_VERSION=13.8.0
RUNNER_SHA=0b573176b26a025a6ac0bbb91cbeb77ce544597b76a5ee53cadae717b66100c9

lint() {
  rpmlint ${CI_PROJECT_DIR}/build/package/rpm/gitlab-runner-ecp.spec
}

build() {
  # rpmdev-setuptree
  mkdir -p ${HOME}/rpmbuild/{BUILD,RPMS,SOURCES,SPECS,SRPMS}

  cp \
    ${CI_PROJECT_DIR}/build/package/rpm/gitlab-runner-ecp.spec \
    ${HOME}/rpmbuild/SPECS/gitlab-runner-ecp.spec

  cp \
    ${CI_PROJECT_DIR}/build/package/rpm/patches/runner_trusted.patch \
    ${HOME}/rpmbuild/SOURCES/runner_trusted.patch

  pushd ${HOME}/rpmbuild
    rpmbuild -ba SPECS/gitlab-runner-ecp.spec \
      --define "_topdir `pwd`" \
      --define "_version ${RUNNER_VERSION}" \
      --define "_sha256sum ${RUNNER_SHA}" \
      --undefine="_disable_source_fetch"
  popd
}

release() {
  mkdir -p ${CI_PROJECT_DIR}/rpms

  cp \
    ${HOME}/rpmbuild/{RPMS/$(uname -m),SRPMS}/* \
    ${CI_PROJECT_DIR}/rpms

  GOARCH=$(go env GOARCH)

  tar \
    -cJf \
    gitlab-runner-ecp-v${RUNNER_VERSION}-${GOARCH}.tar.xz \
    rpms/

  sha256sum gitlab-runner-ecp-v${RUNNER_VERSION}-${GOARCH}.tar.xz
}

case "${1}" in
  ci)
    lint
    build
    ;;
  release)
    build
    release
    ;;
  *)
    echo "Invalid argument: ${0} (ci|release)"
    exit 1
    ;;
esac
