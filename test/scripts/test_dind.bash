#!/usr/bin/env bash

# Execute 'make test' within docker container.
set -eo pipefail
set +o noclobber

[ -z "${ROOT_DIR}" ] && ROOT_DIR=$(pwd)

ci_dir="/builds/ecp-ci/jacamar-ci"

image="golang:1.15.6"
docker pull ${image}

echo "Running Jacamar unit tests..."
docker run \
  -v "${ROOT_DIR}:${ci_dir}" \
  -e "CI_PROJECT_DIR=${ci_dir}" \
  -e "CGO_ENABLED=1" \
  -w "${ci_dir}" \
  -t  "${image}" \
  bash -c " apt-get update \
            && apt-get install -y glibc-source \
            && make test"
