#!/usr/bin/python3

# sequence.py is meant to augment functionality not currently found in Pavilion
# to support our key test workflows. Now all tests need to be suffixed with an
# _<index>. Tests will be run sequentially based upon this index.

import os
import subprocess
import sys
import yaml
from collections import OrderedDict


def orderedSteps(file):
    organized = {}

    with open(r'{0}'.format(file)) as f:
        tests = yaml.full_load(f)
        for k, v in tests.items():
            if k[0] == "_":
                continue
            index = k.find('_')
            if index != -1:
                organized[int(k[index+1:])] = k

    return OrderedDict(sorted(organized.items()))


def executeSteps(test, cwd):
    cmd = 'pav run -r {0} && pav wait -s'.format(test)
    p = subprocess.Popen(cmd,
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True,
        cwd=r'{0}'.format(cwd))
    res = p.wait()

    out,err = p.communicate()
    # We will use 'pav status -a' to organize all results later.
    if err != "":
        print(err)

def main(argv):
    testName = (os.path.basename(argv[1]))[:-5]
    testSteps = orderedSteps(argv[1])

    for k, v in testSteps.items():
        res = executeSteps('{0}.{1}'.format(testName, v), argv[2])


if __name__== "__main__":
    if len(sys.argv) !=  3:
        sys.exit('Error: Specify a Pavilion test file and config directory')

    main(sys.argv)
